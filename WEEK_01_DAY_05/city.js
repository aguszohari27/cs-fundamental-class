const {regencies, provinces} = require('./location');

function findProvince(cityName) {

    const provincies = []

    let regencyId = 0;

    for(let regency of regencies) {
        if(regency.name == cityName) {
            regencyId = regency.province_id;
            break;
        }
    }

    // find city where province_id equals provinceId
    for(let province of provinces) {
        if(province.id == regencyId) {
            provincies.push(province.name);
        }
    }

    return provincies;

}

const cityName = "KABUPATEN CIREBON";
const provincies = findProvince(cityName);
console.log(provincies)
const n1 = 15;
const n2 = 25;
// hasilnya 75

let result = (n1 > n2) ? n1 : n2;

while (true) {
    if (result % n1 == 0 && result % n2 == 0) {
        console.log(`KPK of ${n1} and ${n2} is ${result}`);
        break;
    }
    result++;
}
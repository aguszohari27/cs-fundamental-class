let arr = [60, 72];
let n = arr.length;
// hasilnya 12

function fpb(a, b) {
    if (a == 0)
        return b;
    return fpb(b % a, a);
}

function findFPB(arr, n) {
    let result = arr[0];
    for (let i = 1; i < n; i++) {
        result = fpb(arr[i], result);

        if (result == 1) {
            return 1;
        }
    }
    return result;
}

console.log(findFPB(arr, n));
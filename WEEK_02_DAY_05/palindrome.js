function palindrome(string) {
  let newString = string.split('').reverse().join('');
  if(newString === string){
    return true;
  }
  return false;
}

console.log(palindrome("MATARAMMARATAM"));
console.log(palindrome("palindrome"));
console.log(palindrome("KODOK"));
console.log(palindrome("MALAM"));
console.log(palindrome("tenet"))
console.log(palindrome("mama"));
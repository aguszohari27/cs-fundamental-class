const number = [3, 9, 6, 4, 7, 3, 10, 8, 11];

function run (arr, start, jump) {
    let result = 0

    for (let i = start; i < arr.length; i+=jump) {

        result += arr [i]
    }
    
    return result;

}

const sum = run (number, 2, 2) // hasil yang ditampilkan adalah 34

console.log(sum);
